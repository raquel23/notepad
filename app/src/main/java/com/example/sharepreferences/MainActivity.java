package com.example.sharepreferences;

import android.content.SharedPreferences;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Button buttonSalvar;
    private TextView textResultado;
    private TextInputEditText textoInput;
    private static final String PREFERENCIAS_FILE = "Notas";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textoInput=findViewById(R.id.textoInput);
        textResultado=findViewById(R.id.textViewResultado);

        SharedPreferences preferences;
        SharedPreferences.Editor editor;
        preferences = getSharedPreferences(PREFERENCIAS_FILE, MODE_PRIVATE);
        editor= preferences.edit();
        editor.putString("teste","teste text");
        editor.commit();
        Toast.makeText(this, preferences.getString("teste","erro ao recuperar texto"),Toast.LENGTH_LONG).show();

    }


}
